# Proj_0091

Software der Heizung des Transfersystems mit den Reglern UR4848xA

Use as Header to import package:

#----------------------------------------------------------------#
```python
import sys 
if '$Path_to_proj_0091/UR4848xA_package' not in sys.path:
    sys.path.append('$Path_to_proj_0091/UR4848xA_package')
import Wachendorff_UR4848xA as WUxA
```
#----------------------------------------------------------------#

needs the pymodbus and logging library to work