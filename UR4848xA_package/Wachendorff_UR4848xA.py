# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 09:34:03 2019

@author: tebbe
"""

# ---------------------------------------------------------------------------#
# import needed libraries
# ---------------------------------------------------------------------------#

from pymodbus.compat import IS_PYTHON3, PYTHON_VERSION
if IS_PYTHON3 and PYTHON_VERSION >= (3, 4):
    import logging
    import time
    import threading
    from pymodbus.client.sync import ModbusSerialClient as ModbusClient
else:
    import sys
    sys.stderr("This package needs to be run only on python 3.4 and above")
    sys.exit(1)

# --------------------------------------------------------------------------- #
# configure the client logging
# --------------------------------------------------------------------------- #
logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.WARNING)
#log.setLevel(logging.DEBUG)


# ---------------------------------------------------------------------------#
# Used Modbus Functions

# 0x04 Die Funktion READ INPUT REGISTERS ließt die analogen Eingänge aus 
           # rr = await client.read_input_registers(addr, 1, unit=self.s_addr)
# 0x03 Mit der Funktion READ HOLDING REGISTERS  können die Ein- und Ausgangsworte und die Register gelesen werden.
           # rr = await client.read_holding_registers(addr, 1, unit=self.s_addr)
# 0x06 Mit der Funktion 6 kann auf das Ausgangsprozessabbild und dem Interface zugegriffen werden.
           # rq = client.write_register(addr, selection, unit=self.s_addr)
# ---------------------------------------------------------------------------#
          
           
class Wachendorff_UR4848xA(threading.Thread):
    def __init__(self, slave_address, mod='pid', alarm = 50, decimal_point_selection = 1, port='COM3'):
        self.s_addr = slave_address
        self.modus=0 # if the PID is engaged
        self.alarm_1 = alarm
        self.model_47x = 0
        self.dec_select = decimal_point_selection # for UR4842A only possible to be 0 or 1
        if mod in ['pid','Pid','PID']:
            self.modus=1
        self.windowopen = 1
        self.current_temperature = 0
        self.current_setpoint = 0.0
        self.new_setpoint = 0.0
        self.ramping = False
        self.ramping_speed = 0.0
        threading.Thread.__init__(self)
        self.client = ModbusClient(port=port, baudrate=19200, method="rtu", timeout = 4, bytesize=8, parity='N', stopbits=1)
        
    def connenct(self):
        self.client.connect()
        time.sleep(0.1)
        
    def parameter_set(self):
        model = self.checkmodel()
        if self.modus:
            # selections in Settings start at 0
            if model > 2:
                self.set_Param_to(2000+ 1, 1) # selections of Sensor AI1 starts at 1, since AI2 has disabled mode at 0
                self.set_Param_to(2000+ 2, self.dec_select) # decimal point selection to be 0, 0.0, 0.00, 0.000
                self.set_Param_to(2000+ 35, 4) # Command Output 1 --> 0-10V output AO1
                self.set_Param_to(2000+ 73, 1) # PID 1 --> PID Automatic with input AO1
                self.set_Param_to(2000+ 18, 1) # selections of Sensor AI2 as Tc-K as well as AI1
                self.set_Param_to(2000+ 35, 0) # Command Output 2 --> command disabled
                self.set_Param_to(1208, self.alarm_1) # alarm setpoint
                self.set_Param_to(2000+ 123, 1) # Alarm 1 --> absolute alarm active over
                self.set_Param_to(2000+ 124, 1) # Alarm 1 Value read on imput AI2
                self.set_Param_to(2000+ 128, 9999) # Hysteresis value fahr greater than 0
                self.set_Param_to(2000+ 282, 0) # Digital Voltage Output 12 V (default)
                self.set_Param_to(2000+ 285, 1) # NFC Lock
            else:
                self.set_Param_to(2000+ 1, 1) # selections of Sensor AI1 starts at 1, since AI2 has disabled mode at 0
                self.set_Param_to(2000+ 2, self.dec_select) # decimal point selection to be 0, 0.0, 0.00, 0.000
                self.set_Param_to(2000+ 35, 4) # Command Output 1 --> 0-10V output AO1
                self.set_Param_to(2000+ 282, 0) # Digital Voltage Output 12 V (default)
                self.set_Param_to(2000+ 285, 1) # NFC Lock
        else:
            self.set_Param_to(2000+ 1, 1) # selections of Sensor AI1 starts at 1, since AI2 has disabled mode at 0
            self.set_Param_to(2000+ 2, self.dec_select) # decimal point selection to be 0, 0.0, 0.00, 0.000
            self.set_Param_to(2000+ 35, 4)  # Command Output 1 --> Command on digital output
            self.set_Param_to(2000+ 282, 1) # Digital Voltage Output 24 V
            self.set_Param_to(2000+ 285, 1) # NFC Lock
            self.current_setpoint = self.read_Param(1200)
            self.new_setpoint = self.current_setpoint
        
    def set_setpoint(self, setpoint):
        if round(setpoint,1) != self.current_setpoint:
            self.new_setpoint = round(setpoint,1)
        
    def PID_setpoint_1(self, setpoint):
        model = self.checkmodel()
        if self.modus:
            if model > 2:
                real_setpoint = '{:3.1f}'.format(setpoint)
                self.set_Param_to(int(1006), real_setpoint)
                return setpoint
        else:
            real_setpoint = int(round(setpoint,1)*10) 
            self.set_Param_to(1200, real_setpoint)
            return setpoint
        return 0
    
    def temperatur_1(self):
        return self.temp_read(1)
       
    def temperatur_2(self):
        model = self.checkmodel()
        if model > 2:
            return self.temp_read(2)
        return 0
        
    def checkmodel(self):
        if self.model_47x < 1:
            self.model_47x = -469 + self.read_Param(0) #for UR4842A gives output 471
            log.info("Model UR4848"+str(self.model_47x)+"A")
        return self.model_47x
    
    def set_Param_to(self, addr, selection):
        try:
            log.debug("Write to register "+str(addr)+" the parameter "+str(selection))
            rr = self.client.read_holding_registers(addr, 1, unit=self.s_addr)
            time.sleep(0.01)
            
            if rr.registers[0] != selection:
                rq = self.client.write_register(addr, selection, unit=self.s_addr)
                time.sleep(0.01)
                assert(rq.function_code < 0x80)     # test that we are not an error
                rr = self.client.read_holding_registers(addr, 1, unit=self.s_addr) 
                time.sleep(0.01)
                assert(rr.registers[0]== selection)
                time.sleep(0.01)
        except Exception as e:
            log.info(e)
            
        
    def read_Param(self, addr):
        result = 0
        try:
            log.debug("Read the register "+str(addr))
            rr = self.client.read_holding_registers(addr, 1, unit=self.s_addr)
            time.sleep(0.01)
            result = rr.registers[0]
        except Exception as e:
            log.info(e)
        return result
            
    def temp_read(self, temp_selection):
        addr = 1100
        if (temp_selection==2) :
            addr = 1101
        result = 0
        try:
            log.debug("Read register "+str(addr)+" the temperature "+str(addr-999))
            rr = self.client.read_input_registers(addr, 1, unit=self.s_addr)
            time.sleep(0.01)
            result = rr.registers[0]
        except Exception as e:
            log.info(e)
        return result/10**self.dec_select
    
    def ramping_on(self):
        self.ramping = True
    
    def ramping_off(self):
        self.ramping = False
    
    def set_ramping_speed(self, ramping_speed = 0.0):
        self.ramping_speed = float(ramping_speed)
    
    def run(self):
        self.connenct()
        time.sleep(0.1)
        self.parameter_set()
        time.sleep(0.1)
        interator = 0
        while self.windowopen :
            time.sleep(0.3)
            interator += 1
            if interator > 200 : interator = 0
            if self.ramping and interator > 100:
                interator = 0
                self.set_setpoint(self.current_setpoint + self.ramping_speed * 0.5)
            if self.current_setpoint < 0.0 or self.current_setpoint > 300:
                self.ramping_off()
                
            self.current_temperature = self.temperatur_1()
            if self.current_setpoint != self.new_setpoint :
                self.current_setpoint = self.PID_setpoint_1(self.new_setpoint)
                
        self.client.close()
        time.sleep(0.01)
        
    def close(self):
        self.windowopen = 0

if __name__== "__main__":
    
    regler_1 = Wachendorff_UR4848xA(slave_address = 0xF7, mod = '2state', alarm = 50,port='COM3')
    regler_1.start()
    for i in range(2):
        print(i)
        print(regler_1.current_temperature)
        if i == 1 :
            regler_1.set_setpoint(0)
        time.sleep(1)
    regler_1.close()
    time.sleep(1)
